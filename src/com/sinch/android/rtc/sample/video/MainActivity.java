package com.sinch.android.rtc.sample.video;

import android.app.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.pubnub.api.*;

import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class MainActivity extends BaseActivity {

    static final String JSON_EXCEPTION = "JSON Exception";
    static final String PUBNUB_EXCEPTION = "Pubnub Exception";

    private SinchClient sinchClient;
    private Call currentCall = null;
    private Pubnub pubnub;
    private ArrayList users;
    private JSONArray hereNowUuids;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String username = getIntent().getStringExtra("username");
        pubnub = new Pubnub("pub-c-29cdc63a-f816-42a1-bf09-f48e420947f8", "sub-c-81f60630-fbc8-11e5-b552-02ee2ddab7fe");
        pubnub.setUUID(username);
        sinchClient = Sinch.getSinchClientBuilder()
                .context(this)
                .userId(username)
                .applicationKey("dfb61339-92cb-4045-8b0f-fefbeec017eb")
                .applicationSecret("ghopCTZ+OUq5vBduCBdwtw==")
                .environmentHost("sandbox.sinch.com")
                .build();

        sinchClient.setSupportCalling(true);
        sinchClient.startListeningOnActiveConnection();
        sinchClient.start();

    }

    @Override
    public void onResume() {
        super.onResume();

        users = new ArrayList<String>();
        final ListView usersListView = (ListView) findViewById(R.id.usersListView);
        final ArrayAdapter usersArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.user_list_item, users);
        usersListView.setAdapter(usersArrayAdapter);



        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Call call = getSinchServiceInterface().callUserVideo(users.get(i).toString());
                String callId = call.getCallId();

                Intent callScreen = new Intent(MainActivity.this, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                startActivity(callScreen);
            }
        });

        pubnub.hereNow("calling_channel", new Callback() {
            public void successCallback(String channel, Object response) {
                try {
                    JSONObject hereNowResponse = new JSONObject(response.toString());
                    hereNowUuids = new JSONArray(hereNowResponse.get("uuids").toString());
                } catch (JSONException e) {
                    Log.d(JSON_EXCEPTION, e.toString());
                }

                String currentUuid;
                for (int i = 0; i < hereNowUuids.length(); i++) {
                    try {
                        currentUuid = hereNowUuids.get(i).toString();
                        //if (!currentUuid.equals(pubnub.getUUID())) {
                            users.add(currentUuid);
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    usersArrayAdapter.notifyDataSetChanged();
                                }
                            });
                       // }
                    } catch (JSONException e) {
                        Log.d(JSON_EXCEPTION, e.toString());
                    }
                }
            }

            public void errorCallback(String channel, PubnubError e) {
                Log.d("PubnubError", e.toString());
            }
        });

        try {
            pubnub.subscribe("calling_channel", new Callback() {
            });
        } catch (PubnubException e) {
            Log.d(PUBNUB_EXCEPTION, e.toString());
        }

        try {
            pubnub.presence("calling_channel", new Callback() {

                @Override
                public void successCallback(String channel, Object message) {
                    try {
                        JSONObject jsonMessage = new JSONObject(message.toString());
                        String action = jsonMessage.get("action").toString();
                        String uuid = jsonMessage.get("uuid").toString();

                        if (!uuid.equals(pubnub.getUUID())) {
                            if (action.equals("join")) {
                                users.add(uuid);
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        usersArrayAdapter.notifyDataSetChanged();
                                    }
                                });
                            } else if (action.equals("leave")) {
                                for (int i = 0; i < users.size(); i++) {
                                    if (users.get(i).equals(uuid)) {
                                        users.remove(i);
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                usersArrayAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.d(JSON_EXCEPTION, e.toString());
                    }
                }
            });
        } catch (PubnubException e) {
            Log.d(PUBNUB_EXCEPTION, e.toString());
        }
    }










    @Override
    public void onBackPressed() {
    }

    @Override
    public void onPause() {
        super.onPause();
        pubnub.unsubscribe("calling_channel");
    }


}